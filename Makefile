ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

UNAME_S := $(shell uname -s)

CONFIG	:= ""

ifeq ($(UNAME_S),Linux)
	CONFIG = config/config_linux.toml
endif
ifeq ($(UNAME_S),Darwin)
	CONFIG = config/config_darwin.toml
endif

build:
	go build -o .build/bin/demo demo.go

demo: build
	.build/bin/demo $(CONFIG)

test:
	go test pekan-go/pekan -v

bench:
	go test pekan-go/pekan -bench=.
