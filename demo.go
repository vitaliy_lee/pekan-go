package main

import (
	"fmt"
	"os"

	"pekan-go/pekan"
)

func exitError(message string) {
	fmt.Fprintln(os.Stderr, message)
	os.Exit(1)
}

func handleError(err error) {
	if err != nil {
		exitError(err.Error())
	}
}

func main() {
	if len(os.Args) != 2 {
		exitError(fmt.Sprintf("usage: %s <config_path>", os.Args[0]))
	}

	options, err := pekan.ReadConfig(os.Args[1])
	handleError(err)

	csp, err := pekan.Init(options)
	handleError(err)

	defer func() {
		csp.Finalize()
		fmt.Println("CSP finalized")
	}()

	fmt.Println("CSP initialized")

	message := []byte("Fabrik!Fabrik!")

	digest, err := csp.Hash(message, pekan.SHA256)
	handleError(err)

	fmt.Printf("Digest: %x\n", digest)
}
