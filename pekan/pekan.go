package pekan

import (
	"errors"
	"fmt"
	"os"
	"unsafe"

	"github.com/BurntSushi/toml"
)

/*
#cgo linux CFLAGS: -I/usr/include -Wno-deprecated-declarations
#cgo linux LDFLAGS: -L/usr/lib -lpekan
#include <stdlib.h>
#include <pekan.h>
*/
import "C"

const success = C.SUCCESS

type DigestAlgorithm uint

const (
	SHA256 DigestAlgorithm = 0
	SHA512 DigestAlgorithm = 1
)

type ByteBuffer = C.struct_ByteBuffer

func NewByteBuffer(data []byte) (buffer ByteBuffer) {
	array := C.CBytes(data)

	buffer = ByteBuffer{
		len:  C.long(len(data)),
		data: (*C.uchar)(array),
	}

	return
}

func (buffer C.struct_ByteBuffer) Free() {
	buffer.len = 0
	C.free(unsafe.Pointer(buffer.data))
}

type Options struct {
	Library string `toml:"library"`
	Label   string `toml:"label"`
	Pin     string `toml:"pin"`
}

func ReadConfig(configPath string) (*Options, error) {
	data, err := os.ReadFile(configPath)
	if err != nil {
		err = fmt.Errorf("Failed to read config file %s: %s", configPath, err)
		return nil, err
	}

	var options Options

	_, err = toml.Decode(string(data), &options)
	if err != nil {
		err = fmt.Errorf("Failed to parse config file %s: %s", configPath, err)
		return nil, err
	}

	return &options, nil
}

type CSP struct {
	cspPtr *C.PekanCSP
}

type Signer struct {
	signerPtr *C.PekanSigner
}

func (signer *Signer) Sign(message []byte) (signature []byte, err error) {
	messageBuffer := NewByteBuffer(message)
	defer messageBuffer.Free()

	var (
		signatureBuffer C.struct_ByteBuffer
		extError        C.struct_ExternError
	)

	result := C.pekan_signer_sign(signer.signerPtr, &messageBuffer, &signatureBuffer, &extError)
	if result != success {
		err = errorFrom("failed to sign message", extError)
		return
	}

	signature = C.GoBytes(unsafe.Pointer(signatureBuffer.data), C.int(signatureBuffer.len))
	C.pekan_bytebuffer_free(signatureBuffer)
	return
}

type Verifier struct {
	verifierPtr *C.PekanVerifier
}

func (verifier *Verifier) Verify(signature, message []byte) (valid bool, err error) {
	signatureBuffer := NewByteBuffer(signature)
	defer signatureBuffer.Free()

	messageBuffer := NewByteBuffer(message)
	defer messageBuffer.Free()

	var extError C.struct_ExternError

	result := C.pekan_verifier_verify(verifier.verifierPtr, &signatureBuffer, &messageBuffer, &extError)
	if result == success {
		valid = true
	} else if result == 1 {
		valid = false
	} else {
		err = errorFrom("failed to verify signature", extError)
	}

	return
}

func Init(options *Options) (csp *CSP, err error) {
	library := C.CString(options.Library)
	defer C.free(unsafe.Pointer(library))

	label := C.CString(options.Label)
	defer C.free(unsafe.Pointer(label))

	pin := C.CString(options.Pin)
	defer C.free(unsafe.Pointer(pin))

	opts := C.PekanOptions{
		library: library,
		label:   label,
		pin:     pin,
	}

	var (
		cspPtr   *C.PekanCSP
		extError C.struct_ExternError
	)

	result := C.pekan_init_csp(opts, &cspPtr, &extError)
	if result != success {
		err = errorFrom("failed to init CSP", extError)
		return
	}

	csp = &CSP{cspPtr}
	return
}

func (csp *CSP) Hash(message []byte, algorithm DigestAlgorithm) (hash []byte, err error) {
	messageBuffer := NewByteBuffer(message)
	defer messageBuffer.Free()

	algo := C.PekanDigestAlgorithm(algorithm)

	var (
		hashBuffer C.struct_ByteBuffer
		extError   C.struct_ExternError
	)

	result := C.pekan_hash(csp.cspPtr, &messageBuffer, algo, &hashBuffer, &extError)
	if result != success {
		err = errorFrom("failed to calculate hash", extError)
		return
	}

	hash = C.GoBytes(unsafe.Pointer(hashBuffer.data), C.int(hashBuffer.len))
	C.pekan_bytebuffer_free(hashBuffer)
	return
}

func (csp *CSP) NewSigner(ski []byte, algorithm DigestAlgorithm) (signer *Signer, err error) {
	skiArray := C.CBytes(ski)
	defer C.free(skiArray)

	var skiBuffer C.struct_ByteBuffer
	skiBuffer.len = C.long(len(ski))
	skiBuffer.data = (*C.uchar)(skiArray)

	algo := C.PekanDigestAlgorithm(algorithm)

	var (
		pekanSigner *C.PekanSigner
		extError    C.struct_ExternError
	)

	result := C.pekan_new_signer(csp.cspPtr, &skiBuffer, algo, &pekanSigner, &extError)
	if result != success {
		err = errorFrom("failed to create signer", extError)
		return
	}

	signer = &Signer{pekanSigner}
	return
}

func errorFrom(description string, extError C.struct_ExternError) (err error) {
	message := C.GoString(extError.message)
	C.pekan_string_free(extError.message)
	err = fmt.Errorf(description+": %s", message)
	return
}

func (csp *CSP) NewVerifier(ski []byte, algorithm DigestAlgorithm) (verifier *Verifier, err error) {
	skiArray := C.CBytes(ski)
	defer C.free(skiArray)

	var skiBuffer C.struct_ByteBuffer
	skiBuffer.len = C.long(len(ski))
	skiBuffer.data = (*C.uchar)(skiArray)

	algo := C.PekanDigestAlgorithm(algorithm)

	var (
		pekanVerifier *C.PekanVerifier
		extError      C.struct_ExternError
	)

	result := C.pekan_new_verifier(csp.cspPtr, &skiBuffer, algo, &pekanVerifier, &extError)
	if result != success {
		err = errorFrom("failed to create verifier", extError)
		return
	}

	verifier = &Verifier{pekanVerifier}
	return
}

func (csp *CSP) Finalize() (err error) {
	result := C.pekan_finalize(csp.cspPtr)
	if result != success {
		err = errors.New("Failed to finalize library")
	}

	return
}
