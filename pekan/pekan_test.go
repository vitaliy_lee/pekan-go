package pekan

import (
	"encoding/hex"
	"fmt"
	"os"
	"runtime"
	"testing"
)

var (
	csp         *CSP
	resultValue []byte
	resultError error
)

const testText = "Go written reliable, secure, easy-to-use, and pluggable cryptographic implementations."

func TestMain(m *testing.M) {
	var err error

	configPath := fmt.Sprintf("../config/config_%s.toml", runtime.GOOS)

	options, err := ReadConfig(configPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to read config: %s", err)
		os.Exit(-1)
	}

	csp, err = Init(options)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to init library: %s", err)
		os.Exit(-1)
	}

	fmt.Println("Library initialized")

	exitCode := m.Run()

	err = csp.Finalize()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to finalize library: %s", err)
		os.Exit(-1)
	}

	fmt.Println("Library finalized")

	os.Exit(exitCode)
}

func TestHashSHA256(t *testing.T) {
	const (
		algorithm      = SHA256
		message        = "Fabrik!Fabrik!"
		expectedDigest = "e586b6e411722d4f22abe83f55b67e057750968d53a1883087777548389aba27"
	)

	digest, err := csp.Hash([]byte(message), algorithm)
	if err != nil {
		t.Fatal(err)
	}

	digestStr := hex.EncodeToString(digest)

	if digestStr != expectedDigest {
		t.Fatalf("digest %s is not as expected %s", digestStr, expectedDigest)
	}
}

func TestHashSHA512(t *testing.T) {
	const (
		algorithm      = SHA512
		message        = "Fabrik!Fabrik!"
		expectedDigest = "363d6480fe445e2f26e66163af16fe99570c8cb10e3097648c4ba1f737174ce37b5776b2e1239e5c571b17eda70769ef042c199d450e267c91ba48eb4f34c0b1"
	)

	digest, err := csp.Hash([]byte(message), algorithm)
	if err != nil {
		t.Fatal(err)
	}

	digestStr := hex.EncodeToString(digest)

	if digestStr != expectedDigest {
		t.Fatalf("digest %s is not as expected %s", digestStr, expectedDigest)
	}
}

func TestSignVerify(t *testing.T) {
	const (
		skiText   = "e586b6e411722d4f22abe83f55b67e057750968d53a1883087777548389aba27"
		algorithm = SHA256
	)

	ski, err := hex.DecodeString(skiText)
	if err != nil {
		t.Fatal(err)
	}

	signer, err := csp.NewSigner(ski, algorithm)
	if err != nil {
		t.Fatal(err)
	}

	if signer == nil {
		t.Fatal("signer must not be nil")
	}

	message := []byte(testText)

	signature, err := signer.Sign(message)
	if err != nil {
		t.Fatal(err)
	}

	if len(signature) == 0 {
		t.Fatal("signature must not be empty")
	}

	verifier, err := csp.NewVerifier(ski, algorithm)
	if err != nil {
		t.Fatal(err)
	}

	if verifier == nil {
		t.Fatal("verifier must not be nil")
	}

	valid, err := verifier.Verify(signature, message)
	if err != nil {
		t.Fatal(err)
	}

	if !valid {
		t.Fatal("Signature must be valid")
	}
}

func BenchmarkHashSHA256(b *testing.B) {
	benchmarkHash(b, SHA256)
}

func BenchmarkHashSHA512(b *testing.B) {
	benchmarkHash(b, SHA512)
}

func benchmarkHash(b *testing.B, algorithm DigestAlgorithm) {
	message := []byte(testText)

	var (
		digest []byte
		err    error
	)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		digest, err = csp.Hash(message, algorithm)
	}

	resultValue = digest
	resultError = err
}
